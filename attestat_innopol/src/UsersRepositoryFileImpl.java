import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl {

    private List<User> usersList;

    public UsersRepositoryFileImpl() {
        this.usersList = new ArrayList<>();
    }


    public User findById(int id) {
        for (User user : usersList) {
            if (user.getId() == id) {

                return user;
            }
        }
        System.out.println("нет данных под таким id ");
        return null;
    }

    public void create(User user) {

        if(usersList.stream().map(User::getId).anyMatch(id -> id.equals(user.getId()))){
            System.out.println("Ошибка! Пользователь с таким идентификациооным номером уже существует, привяжите новое Id");
        }else{
            usersList.add(user);


        }
    }


    public void update(User user) {
        int index = usersList.size();
        for (int i = 0; i < usersList.size(); i++) {
            if (usersList.get(i).getId().equals(user.getId())) {
                index = i;
            }
        }
        if (index != usersList.size()) {
            usersList.remove(index);
        }
        usersList.add(user);
    }


    public void delete(int id) {
        for (int i = 0; i < usersList.size(); i++) {
            if (usersList.get(i).getId() == id) {
                usersList.remove(usersList.get(i));
            }

        }
    }

public List<User> getUsersList(){
        return usersList;
}
}

