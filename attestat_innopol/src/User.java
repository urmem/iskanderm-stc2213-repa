public class User {
    private Integer id;
    private String name;
    private String surname;
    private Integer age;
    private boolean work;

    public User(Integer id, String name, String surname, Integer age, boolean work) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.work = work;
    }

    public User(Integer id, String name, String surname, Integer age, String work) {
       this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.work = true;
    }

    public User(Integer id, String name, String surname, Integer age) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.work = false;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        if (id < 1) {
            System.out.println("Неверное id, оно не может быть меньше 1-го");
        } else {
            this.id = id;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.isEmpty()) {
            System.out.println("Нужно написать имя");
        } else {
            this.name = name;
        }
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        if (surname == null || surname.length() < 2) {
            System.out.println("Напишите корректно фамилию");
        } else {
            this.surname = surname;
        }
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        if (age < 0) {
            System.out.println("Возраст не может быть меньше нуля");
        } else {
            this.age = age;
        }
    }

    public boolean isWork() {
        return work;
    }

    public void setWork(Boolean work) {

        this.work = work;

    }
//public String toFile(){
//        return (getId()+ ";" + getName() + ";" + getSurname() + ";" + getAge() + ";" + Boolean+ ";" + "\n");
//}
//
//    public String toFile(){
//        return (getId()+ ";" + getName() + ";" + getSurname() + ";" + getAge() + ";" + "\n");
//    }
    @Override
    public String toString() {
        return ("Данные человека: \n" +
                "1)id - " + getId() + "\n" +
                "2)имя - " + getName() + "\n" +
                "3)фамилия - " + getSurname() + "\n" +
                "4)возраст - " + getAge() + "\n" +
                "5)наличие работы - " + isWork()) + "\n" +
                "****************************************";
    }

    public String dataRecordingLogic() {
        return getId() + ";" + getName() + ";" + getSurname() + ";" + getAge() + ";" + isWork() + "\n";
    }
}