import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {


        UsersRepositoryFileImpl usersRepositoryFile = new UsersRepositoryFileImpl();
        File file = new File("src\\usersList\\users.txt");

        String initialData = "1;Durov;Pavel;37;" + System.lineSeparator() +
                "2;Vladimir;Levin;55;hacker" + System.lineSeparator() +
                "3;Vladimir;Putin;69;the president" + System.lineSeparator() +          //I do not know how he got here...
                "4;Evgeny;Bogachev;46;hacker" + System.lineSeparator() +
                "5;Evgeny;Kaspersky;57;the world's leading specialist in the field of information security" + System.lineSeparator() +
                "6;Kevin;Mitnic;59;hacker" + System.lineSeparator() +
                "7;Elon;Musk;51;entrepreneur, engineer and billionaire" + System.lineSeparator();

        try {
            FileWriter writer = new FileWriter(file);
            writer.write(initialData);
            writer.close();
        } catch (IOException ex) {
            System.out.println("Файл не найден");

        }
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
            while ((scanner.hasNext())) {
                String[] temp = scanner.nextLine().split(";");
                User user = null;
                if (temp.length == 4) {
                    user = new User(Integer.parseInt(temp[0]),
                            temp[1], temp[2],
                            Integer.parseInt(temp[3]));
                } else {
                    user = new User(Integer.parseInt(temp[0]),
                            temp[1], temp[2],
                            Integer.parseInt(temp[3]),
                            (temp[4]));
                }
                usersRepositoryFile.create(user);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
        System.out.println("***********************CREATE*******************\n");
        usersRepositoryFile.create(new User(8, "Gerald", "IzRivii", 45, "Vedmak"));
        System.out.println(usersRepositoryFile.findById(8));

        System.out.println("***********************findById*******************\n");
        User user1 = usersRepositoryFile.findById(4);
        System.out.println(user1);
        user1.setAge(32);
        user1.setName("Stepan");
        System.out.println("***********************UPDATE*******************\n");

        usersRepositoryFile.update(user1);

        System.out.println(user1);

        usersRepositoryFile.delete(2);
        System.out.println("***********************DELETE*******************\n");
        System.out.println(usersRepositoryFile.findById(2));
        System.out.println();

        FileWriter usersFile = null;
        try {
            usersFile = new FileWriter("src\\usersList\\usersOK.txt");
            for (User user : usersRepositoryFile.getUsersList()) {
                usersFile.append(user.dataRecordingLogic());
            }
            usersFile.flush();
        } catch (IOException e) {
            System.out.println("Файл не создан");
        } finally {
            try {
                usersFile.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

    }
}


