/*Почему программисты не живут отдельно от родителей?
        - Не хотят чтобы отключилась функция авто заполнения холодильника.
 */


import com.sun.jdi.Value;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.Key;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Main {


    public static void main(String[] args) {
        HashMap<String, Integer> map = new HashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String string = null;
        System.out.println("Введите предложение");
        try {
            string = reader.readLine();
            System.out.println(string);
        } catch (IOException e) {
            System.out.println("Введите пожалуйста строку");
        }
        String[] words = string.split(" ");
        for (int i = 0; i < words.length; i++) {
            if (!map.containsKey(words[i])) {
                map.put(words[i], 1);
            } else {
                int count = map.get(words[i]);
                map.put(words[i], count + 1);
            }
        }
        if (!map.isEmpty()) {
            for (Map.Entry<String, Integer> element : map.entrySet()) {
                System.out.println(element.getKey() + " = " + element.getValue());
            }
        }

    }
}


