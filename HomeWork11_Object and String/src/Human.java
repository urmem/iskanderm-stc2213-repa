import java.util.Objects;

public class Human {
    private String name;
    private String lastName;
    private String patronymic;
    private String city;
    private String street;
    private String house;
    private String flat;
    private String numberPassport;

    public Human(String name, String lastName, String patronymic, String city, String street, String house, String flat,
                 String numberPassport) {

        setName(name);
        setLastName(lastName);
        setPatronymic(patronymic);
        setCity(city);
        setStreet(street);
        setHouse(house);
        setFlat(flat);
        setNumberPassport(numberPassport);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.isEmpty()) {
            System.out.println("ВВЕДИТЕ ИМЯ!!!");
        } else {
            this.name = name;
        }
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName.isEmpty()) {
            System.out.println("ВВЕДИТЕ ФАМИЛИЮ!!!");
        } else {
            this.lastName = lastName;
        }
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        if (patronymic.isEmpty()) {
            System.out.println("ВВЕДИТЕ ОТЧЕСТВО!!!");
        } else {
            this.patronymic = patronymic;
        }
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        if (city.isEmpty()) {
            System.out.println("ВВЕДИТЕ ГОРОД!!!");
        } else {
            this.city = city;
        }
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        if (street.isEmpty()) {
            System.out.println("ВВЕДИТЕ УЛИЦУ!!!");
        } else {
        }
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        if (house.isEmpty()) {
            System.out.println("ВВЕДИТЕ НОМЕР ДОМА");
        } else {
            this.house = house;
        }
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        if (flat.isEmpty()) {
            System.out.println("ВВЕДИТЕ НОМЕР КВАРТИРЫ");
        } else {
            this.flat = flat;
        }
    }

    public String getNumberPassport() {
        return numberPassport;
    }

    public void setNumberPassport(String numberPassport) {
        if (numberPassport.isEmpty()) {
            System.out.println("ВВЕДИТЕ НОМЕР И СЕРИЮ ПАСПОРТА!!!");
        } else {
            this.numberPassport = numberPassport;
        }
    }

    @Override
    public String toString() {
        String strings = (getLastName() + " " + getName() + " " + getPatronymic()) + System.lineSeparator() +
                ("Паспорт:") + System.lineSeparator() +
                ("Серия " + getNumberPassport().substring(0, 4) + " " + "Номер" + " " +
                        getNumberPassport().substring(5, getNumberPassport().length())) + System.lineSeparator() +
                ("Город" + " " + getCity() + "," + " " + "ул." + getStreet() + "," + " " +
                        "дом " + getHouse() + ", " + "квартира " + getFlat());
        return strings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return numberPassport.substring(0,4).equals(human.numberPassport.substring(0,4));
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberPassport);
    }
}


