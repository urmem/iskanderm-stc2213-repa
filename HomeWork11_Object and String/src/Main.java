public class Main {
    public static void main(String[] args) {
        Human human1 = new Human("Святогор", "Прохоров", "Игоревич", "Новгород", "Старейшин",
                "13", "3", "1300 909090"); // создаем объект Human
        System.out.println(human1.toString()); //Выводим паспортные данные личности
        System.out.println("********************************************");
        Human human2 = new Human("Кречет", "Мирный", "Иванович", "Псков", "Садовая",
                "25", "9", "1304 908978"); //создаем объект Human
        System.out.println(human2.toString()); // Выводим паспортные данные личности
        System.out.println();

        System.out.println(human1.equals(human2));
    }
}
