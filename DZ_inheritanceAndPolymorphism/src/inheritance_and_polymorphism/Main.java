package inheritance_and_polymorphism;

public class Main {
    public static void main(String[] args) {
        System.out.println("***************************************************************************");
        System.out.println();
        Worker workerR = new Raf("Rafael", "Ninja", "Сисадмин", false, 23);
        workerR.goToWork();
        workerR.goToVacation(23);
        System.out.println("***************************************************************************");
        System.out.println();


        System.out.println("***************************************************************************");
        System.out.println();

        Worker workerM = new Mike("Mike", "Ninja", "Программист", true, 15);
        workerM.goToWork();
        workerM.goToVacation(15);
        System.out.println("***************************************************************************");
        System.out.println();


        System.out.println("***************************************************************************");
        System.out.println();
        Worker workerL = new Leo("Leonardo", "Ninja", "Девопс", true, 20);
        workerL.goToWork();
        workerM.goToVacation(20);
        System.out.println("***************************************************************************");
        System.out.println();


        System.out.println("***************************************************************************");
        System.out.println();
        Worker workerD = new Don("Donatello", "Ninja", "Тестировщик", false, 17);
        workerD.goToWork();
        workerD.goToVacation(17);
        System.out.println("***************************************************************************");
    }

}
