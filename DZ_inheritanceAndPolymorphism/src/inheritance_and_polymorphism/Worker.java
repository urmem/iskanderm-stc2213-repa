package inheritance_and_polymorphism;

/*Работа программиста и шамана имеет много общего - оба бормочут непонятные
слова, совершают непонятные действия и не могут объяснить, как оно работает.*/


public class Worker {
    private String name; // имя

    private String lastName; // фамилия
    private String profession; // профессия

    //boolean duration; // флажок ставки работника


    public Worker(String name, String lastName, String profession) {
        this.name = name;
        this.lastName = lastName;
        this.profession = profession;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }


    public void goToWork() {
        System.out.println("На данный момент работает неопределенный работник");


    }


    public void goToVacation(int days) {
        System.out.println("кто уходит в отпуск, какая у него профессия и на сколько дней уходит в отпуск");

    }

    ;


}
