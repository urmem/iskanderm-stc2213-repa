package inheritance_and_polymorphism;

public class Raf extends Worker {
    boolean efficiencyFactor;
    private int vacationDay = 24;

    public Raf(String name, String lastName, String profession, boolean duration, int vacationDay) {
        super(name, lastName, profession);
        this.efficiencyFactor = duration;
        this.vacationDay = vacationDay;
    }

    @Override
    public void goToWork() {
        System.out.println("Сегодня работает " + getLastName() + " " + getName());
        if (efficiencyFactor) {
            System.out.println("Этот работник  может в Си и у него есть допуск к главной ветке от нашего проекта, "
                    + " не любит большое скопление людей");
        } else {
            System.out.println("Этот работник хорошо разбирается в железе и у него есть ключи от серверной, " +
                    "хорошо коммуницирует с обществом");
        }

    }

    @Override
    public void goToVacation(int days) {
        System.out.println(getName() + " " + getLastName() + " идет в отпуск, " + " и Мы " + "на "
                + days + " дня остаемся без " + getProfession() + "а");
    }
}
