package inheritance_and_polymorphism;

public class Don extends Worker {
    boolean efficiencyFactor;
    int vacationDay;

    public Don(String name, String lastName, String profession, boolean efficiencyFactor, int vacationDay) {
        super(name, lastName, profession);
        this.efficiencyFactor = efficiencyFactor;
        this.vacationDay = vacationDay;
    }

    @Override
    public void goToWork() {
        System.out.println("Сегодня работает " + getLastName() + " " + getName());
        if (efficiencyFactor) {
            System.out.println("Этот работник  может в Си и у него есть допуск к главной ветке от нашего проекта");
        } else {
            System.out.println("Этот работник хорошо разбирается в железе и у него есть ключи от серверной.");
        }
    }


    @Override
    public void goToVacation(int days) {
        System.out.println(getName() + " " + getLastName() + " идет в отпуск, " + " и Мы " + "на "
                + days + " остаемся без " + getProfession() + "а");
    }
}
