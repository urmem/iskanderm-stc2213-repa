package dz5_2.src.src.human;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        int rand = random.nextInt(3, 6) + 1;            //рандоное число для будущего массива


        System.out.println("*********************************************");
        System.out.println("** Размер для создающегося массива-" + rand + " ячеек **");
        System.out.println("*********************************************");

        for (int y = 0; y < 5; y++)
        {
            System.out.println();
        }



        Human[] humans = new Human[rand];           //массив humans с объектами Human

        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human("homoSapiens" + i, "LM" + i, random.nextInt(5));


        }
        System.out.println("************************");
        System.out.println("объекты Human");
        System.out.println();
        for (int i = 0; i < humans.length; i++) {
            System.out.println(humans[i]);

        }
        System.out.println("*************************");


        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int x = 1; x < humans.length; x++) {
                if (humans[x].getAge() < humans[x - 1].getAge()) {
                    Human temp = humans[x];
                    humans[x] = humans[x - 1];
                    humans[x - 1] = temp;
                    isSorted = false;


                }

            }

        }
        System.out.println();
        System.out.println();
        System.out.println("Отсортированные люди по возрасту");
        System.out.println("***************************************************************************" +
                "*****************************************");
        System.out.println(Arrays.toString(humans));
        System.out.println("****************************************************************************" +
                "*****************************************");
    }

}
