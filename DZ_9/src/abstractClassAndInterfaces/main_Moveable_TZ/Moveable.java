package abstractClassAndInterfaces.main_Moveable_TZ;

public interface Moveable {
    void move(int x, int y);
}
