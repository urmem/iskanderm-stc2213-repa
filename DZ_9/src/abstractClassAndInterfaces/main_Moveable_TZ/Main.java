package abstractClassAndInterfaces.main_Moveable_TZ;

import abstractClassAndInterfaces.Figur.*;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Figure> figures = new ArrayList<>();
        figures.add(new Ellipse(10, 7));
        figures.add(new Circle(16, 10));
        figures.add(new Rectangle(10, 2));
        figures.add(new Square(5, 4));


        for (Figure figur : figures) {
            if (figur instanceof Moveable) {
                figur.getPerimeter();
                ((Moveable) figur).move(89, 90);
            } else {
                figur.getPerimeter();
            }


        }

    }
}