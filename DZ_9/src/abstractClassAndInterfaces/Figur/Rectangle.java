package abstractClassAndInterfaces.Figur;

import abstractClassAndInterfaces.Figur.Figure;

public class Rectangle extends Figure {


    public Rectangle(int x, int y) {
        super(x, y);
    }

    @Override
    public void getPerimeter() {
        System.out.println("Периметр прямоугольника равен = " + (getX() + getY()) * 2); //P = 2 · (a + b)

    }
}
