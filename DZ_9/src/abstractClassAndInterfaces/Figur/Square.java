package abstractClassAndInterfaces.Figur;

import abstractClassAndInterfaces.main_Moveable_TZ.Moveable;

public class Square extends Rectangle implements Moveable {


    @Override
    public int getY() {
        return 0;
    }

    public Square(int x, int y) {
        super(x, y);
    }

    @Override
    public void getPerimeter() {
        System.out.println("Периметр квадрата равен " + getX() * 4); //P = 4a
    }

    @Override
    public void move(int x, int y) {
        System.out.println("Перемещаем точки квадрата = " + (getX() + (Math.random() * 12)) * 4);
    }
}
