package abstractClassAndInterfaces.Figur;

public class Ellipse extends Figure {

    public Ellipse(int x, int y) {
        super(x, y);
    }

    @Override
    public void getPerimeter() {
        System.out.println("Периметр фигуры эллипс равен = " + 2 * Math.PI * (Math.sqrt((getX() * getX())
                + (getY() * getY())) / 2));  // по формуле    P= 2π √ (a 2+ b 2)/ 2
    }
}
