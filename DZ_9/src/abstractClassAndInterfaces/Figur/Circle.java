package abstractClassAndInterfaces.Figur;

import abstractClassAndInterfaces.main_Moveable_TZ.Moveable;


//для нахождения периметра достаточно ввода данных радиуса. В  ТЗ не написано -
//кто и как задает данные, поэтому я не стал писать условия  или перехват пустого
//ввода данных периметра.
public class Circle extends Ellipse implements Moveable {

    @Override
    public int getY() {
        return 0;
    }

    public Circle(int x, int y) {
        super(x, y);
    }

    @Override
    public void getPerimeter() {
        System.out.println("Периметр круга равен = " + 2 * Math.PI * getX());    //находим периметр по формуле (P = 2πR)
    }

    @Override
    public void move(int x, int y) {
        System.out.println("Меняем позицию круга, меняем точку Х  " + getX() + x + " и точка У " + getY() + y);
    }
}
