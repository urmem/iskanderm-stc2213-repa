package dz5_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class SlognostAlgoritma {

    public static void main(String[] args) {


        ArrayList<Integer> newList = new ArrayList<>(); // создаем массив

        int numberNegativ = (-101); // флажок

        int a = 0;


        System.out.println("Введите число от -100 до 100");
        a = inputNumbX(/*отсюда а можно убрать*/);              //считываем ввод
        while (a != numberNegativ){                 //идет ввод пока не срабатывает флажок
            newList.add(a);
            a = inputNumbX();
        }




        int min = Integer.MAX_VALUE;
        int minimIndex = 0;
        for(int i = 0; i < newList.size();i++){
            if (newList.get(i) < min && newList.get(i) != 0){
                min = newList.get(i);
                minimIndex = i;
            }
        }
        System.out.println("Число с минимальным кол-вом повторений " + (newList.get(minimIndex)));

    }




    public static int inputNumbX() {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            int a = Integer.parseInt(reader.readLine());
            if (a <= 100 && a >= -101) {
                return a;
            }
        } catch (IOException | NumberFormatException e) {
            System.out.println("Введите целое число от -100 до 100");
        }
        return inputNumbX();
    }
}


